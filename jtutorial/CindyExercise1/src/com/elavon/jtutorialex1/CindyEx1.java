package com.elavon.jtutorialex1;

public class CindyEx1 {

	public static void main(String[] args) {
		System.out.println(" ***   *  *   *  ****   *****  ****   *****  *      *        *");
		System.out.println("*   *  *  **  *  *   *  *      *   *  *      *      *       * *");
		System.out.println("*      *  * * *  *   *  ***    ****   ***    *      *      *****");
		System.out.println("*   *  *  *  **  *   *  *      *  *   *      *      *      *   *");
		System.out.println(" ***   *  *   *  ****   *****  *   *  *****  *****  *****  *   *");
		System.out.println();
		System.out.println("****   ****   *   ****   *****  *   *  *   ***");
		System.out.println("*   *  *   *  *  *       *      **  *  *  *   *");
		System.out.println("****   ****   *   ****   ***    * * *  *  *   *");
		System.out.println("*   *  *  *   *       *  *      *  **  *  *   *");
		System.out.println("****   *   *  *   ****   *****  *   *  *   ***");
		System.out.println();
		System.out.println("****   *****  *        *");
		System.out.println("*   *  *      *       * *");
		System.out.println("*   *  ***    *      *****");
		System.out.println("*   *  *      *      *   *");
		System.out.println("****   *****  *****  *   *");
		System.out.println();
		System.out.println(" ***   ****   *   *  *****");
		System.out.println("*   *  *   *  *   *     *");
		System.out.println("*      ****   *   *    *");
		System.out.println("*   *  *  *   *   *   *");
		System.out.println(" ***   *   *   ***   *****");
	}

}
