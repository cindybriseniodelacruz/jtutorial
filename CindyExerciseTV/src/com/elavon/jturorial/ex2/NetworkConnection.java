package com.elavon.jturorial.ex2;

public interface NetworkConnection{
	public NetworkConnection connect(String networkName);
	public boolean connectionStatus();	

}
