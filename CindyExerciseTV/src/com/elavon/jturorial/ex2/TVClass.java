package com.elavon.jturorial.ex2;

public class TVClass {
	protected String brand;
	protected String model;
	protected boolean powerOn;
	protected int channel;
	protected int volume;

	public TVClass(String brand, String model) {
		this.brand = brand;
		this.model = model;
		this.powerOn = false;
		this.channel = 0;
		this.volume = 5;
	}

	public void turnOff() {
		this.powerOn = false;
	}

	public void turnOn() {
		this.powerOn = true;
	}

	public void channelUp() {
		this.channel = this.channel + 1;
	}

	public void channelDown() {
		this.channel = this.channel - 1;
	}

	public void volumeUp() {
		this.volume = this.volume + 1;
	}

	public void volumeDown() {
		this.volume = this.volume - 1;

	}
	
	public String toString(){
		return brand + " " + model + " [powerOn:" + powerOn + ", channel:" + channel + ", volume:" + volume + "]";
	}
	public static void main(String[] args){
		TVClass tv = new TVClass("Andre Electronics", "ONE");
		System.out.println(tv);
		tv.turnOn();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelUp();
		tv.channelDown();
		tv.volumeDown();
		tv.volumeDown();
		tv.volumeDown();
		tv.volumeUp();
		tv.turnOff();
		System.out.println(tv);
		
	}
}
