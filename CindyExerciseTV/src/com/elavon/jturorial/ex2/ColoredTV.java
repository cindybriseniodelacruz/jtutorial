package com.elavon.jturorial.ex2;

public class ColoredTV extends TVClass {
	private int brightness;
	private int contrast;
	private int picture;

	public ColoredTV(String brand, String model) {
		super(brand, model);
		this.brightness = 50;
		this.contrast = 50;
		this.picture = 50;

	}

	public void brightnessUp() {
		this.brightness = this.brightness + 1;
	}

	public void brightnessDown() {
		this.brightness = this.brightness - 1;
	}

	public void contrastUp() {
		this.contrast = this.contrast + 1;
	}

	public void contrastDown() {
		this.contrast = this.contrast - 1;
	}

	public void pictureUp() {
		this.picture = this.picture + 1;
	}

	public void pictureDown() {
		this.picture = this.picture - 1;
	}

	public void swtichToChannel(int channel) {
		this.channel = channel;
	}

	public void mute() {
		this.volume = 0;
	}

	public String toString() {
		return super.toString() + "[b:" + brightness + ", c:" + contrast
				+ ", p:" + picture + "]";
	}

	public static void main(String[] args) {

		ColoredTV bnwTV = null;
		ColoredTV sonyTV = null;
		
		bnwTV = new ColoredTV("Admiral", "A1");
		sonyTV = new ColoredTV("SONY", "S1");
		
		System.out.println(bnwTV);
		System.out.println(sonyTV);
		sonyTV.brightnessUp();
		System.out.println(sonyTV);
		
		ColoredTV sharpTV = new ColoredTV("SHARP", "SH1");
		sharpTV.mute();
		System.out.println(sharpTV);
		sharpTV.brightnessUp();
		sharpTV.brightnessUp();
		sharpTV.contrastDown();
		sharpTV.contrastDown();
		sharpTV.contrastDown();
		sharpTV.contrastDown();
		sharpTV.pictureUp();
		sharpTV.pictureUp();
		System.out.println(sharpTV);
	}
	
	
}
