package com.elavon.training.impl;

import com.elavon.training.intrface.AwesomeCalculator;

public class AwesomeCalculatorImpl implements AwesomeCalculator {

	@Override
	public int getSum(int augend, int addend) {
		int sum = augend + addend;
		return sum;
	}

	@Override
	public double getDifference(double minuend, double subtrahend) {
		double difference = minuend - subtrahend;
		return difference;
	}

	@Override
	public double getProduct(double multiplicand, double multiplier) {
		double product = multiplicand * multiplier;
		return product;
	}

	@Override
	public String getQuotientAndRemainder(double dividend, double divisor) {
		int quotient = (int) (dividend / divisor);
		double remainder = dividend % divisor;
		String quorem = "quotient: " + quotient + " remainder " + remainder;
		return quorem;
	}

	@Override
	public double toCelsius(int fahrenheit) {
		double celcius = (fahrenheit - 32) * (5 / 9);
		return celcius;
	}

	@Override
	public double toFahrenheit(int celsius) {
		double fahrenheit = (celsius + (9 / 5)) + 32;
		return fahrenheit;
	}

	@Override
	public double toKilogram(double lbs) {
		double kilogram = lbs * 0.454;
		return kilogram;
	}

	@Override
	public double toPound(double kg) {
		double pound = kg / 0.454;
		return pound;
	}

	@Override
	public boolean isPalindrome(String str) {
		int n = str.length();
		for (int i = 0; i < (n / 2); ++i) {
			if (str.charAt(i) != str.charAt(n - i - 1)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int getSum(int... summands) {
		int sum = 0;
		for (int x : summands) {
			sum = sum + x;
		}
		return sum;
	}

}
