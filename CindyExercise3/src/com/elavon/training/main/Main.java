package com.elavon.training.main;

import com.elavon.training.impl.AwesomeCalculatorImpl;
import com.elavon.training.intrface.AwesomeCalculator;

public class Main {

	public static void main(String[] args) {

		AwesomeCalculator awesomeCalculator = new AwesomeCalculatorImpl();
		int sum = awesomeCalculator.getSum(5, 6);
		int summands = awesomeCalculator.getSum(0, 1, 2, 3, 4, 5, 6, 7, 8);
		double difference = awesomeCalculator.getDifference(14, 7);
		double product = awesomeCalculator.getProduct(35,3);
		String quorem = awesomeCalculator.getQuotientAndRemainder(25, 6);
		double celsius = awesomeCalculator.toCelsius(120);
		double fahrenheit = awesomeCalculator.toFahrenheit(37);
		double kilogram = awesomeCalculator.toKilogram(90);
		double pound = awesomeCalculator.toPound(54);
		String str = "alaala";
		boolean isPalindrome = awesomeCalculator.isPalindrome(str);
		
		
		System.out.println("sum: " + sum);
		System.out.println("Summands: " + summands);
		System.out.println("difference: " + difference);
		System.out.println("product: " + product);
		System.out.println(quorem);
		System.out.println("Fahrenheit to Celsius: " + celsius);
		System.out.println("Celsius to Fahrenheit: " + fahrenheit);
		System.out.println("pounds to kilograms: " + kilogram);
		System.out.println("kilograms to pounds: " + pound);
		System.out.println("Is " + str + " a Palndrome?: " + isPalindrome);
	
	}

}
