package com.elavon.jtutorial.ex4;

public class TwelveDaysOfChristmas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for (int counter = 1; counter <= 12; counter++) {

			String days;
			String thirdLine = null;
			if (counter == 1) {
				days = counter +  "st";
				thirdLine = "A Partridge in a Pear Tree";
			} else if (counter == 2) {
				days = counter + "nd";
				thirdLine = "2 Turtle Doves \nand A Partridge in a Pear Tree";
			} else if (counter == 3){
				days = counter + "rd";
				thirdLine = "3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 4){
				days = counter + "th";
				thirdLine = "4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 5){
				days = counter + "th";
				thirdLine = "5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 6){
				days = counter + "th";
				thirdLine = "6 Geese a Laying \n5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 7){
				days = counter + "th";
				thirdLine = "7 Swans a Swimming \n6 Geese a Laying \n5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 8){
				days = counter + "th";
				thirdLine = "8 Maids a Milking \n7 Swans a Swimming \n6 Geese a Laying \n5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 9){
				days = counter + "th";
				thirdLine = "9 Ladies Dancing \n8 Maids a Milking \n7 Swans a Swimming \n6 Geese a Laying \n5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 10){
				days = counter + "th";
				thirdLine = "10 Lords a Leaping \n9 Ladies Dancing \n8 Maids a Milking \n7 Swans a Swimming \n6 Geese a Laying \n5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else if (counter == 11){
				days = counter + "th";
				thirdLine = "11 Pipers Piping \n10 Lords a Leaping \n9 Ladies Dancing \n8 Maids a Milking \n7 Swans a Swimming \n6 Geese a Laying \n5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			else{
				days = counter + "th";
				thirdLine = "12 Drummers Drumming \n11 Pipers Piping \n10 Lords a Leaping \n9 Ladies Dancing \n8 Maids a Milking \n7 Swans a Swimming \n6 Geese a Laying \n5 Golden Rings \n4 Calling Birds \n3 French Hens \n2 Turtle Doves \nand A Partridge in a Pear Tree";
			}
			System.out.println("On the " + days + " day of Christmas");
			System.out.println("my true love sent to me: ");
			System.out.println(thirdLine + "\n");

		}

	}

}
